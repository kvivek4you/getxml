var http = require('http');
var express = require('express');
var router = express.Router();
var request = require('request');
var xml = require('xml');
var JSFtp = require("jsftp");
var Ftp = new JSFtp({
  host: "",
  //port: 3331, // defaults to 21
  user: "", // defaults to "anonymous"
  pass: "" // defaults to "@anonymous"
});

/* GET home page. */
router.get('/', function(req, res, next) {
	Ftp.get('server.html', 'download/local.html', function(hadErr) {
    if (hadErr)
      console.error('There was an error retrieving the file.');
    else
      console.log('File copied successfully!');
  });
	
});

module.exports = router;
